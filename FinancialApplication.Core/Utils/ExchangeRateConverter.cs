using System.Globalization;
using FinancialApplication.Core.Models;
using FinancialApplication.Core.Utils.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FinancialApplication.Core.Utils
{
    public class ExchangeRateConverter : IDataConverter<ExchangeRateResponse>
    {
        private readonly JsonSerializerSettings _serializerSettings;

        public ExchangeRateConverter(JsonSerializerSettings serializerSettings = null)
        {
            _serializerSettings = serializerSettings ?? GetDefaultSerializeSettings();
        }

        private JsonSerializerSettings GetDefaultSerializeSettings()
            => new()
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                Converters =
                {
                    new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
                },
            };

        public ExchangeRateResponse Convert(string data)
            => JsonConvert.DeserializeObject<ExchangeRateResponse>(data, _serializerSettings);
    }
}