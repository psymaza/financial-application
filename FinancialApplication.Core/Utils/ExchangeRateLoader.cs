using System;
using System.Net.Http;
using System.Threading.Tasks;
using FinancialApplication.Core.Models;
using FinancialApplication.Core.Utils.Interfaces;

namespace FinancialApplication.Core.Utils
{
    public class ExchangeRateLoader : ILoader<ExchangeRateResponse, string>
    {
        private readonly Uri _url;
        private readonly IDataConverter<ExchangeRateResponse> _converter;

        public ExchangeRateLoader(string url, IDataConverter<ExchangeRateResponse> converter)
        {
            _url = new Uri(url);
            _converter = converter;
        }

        public async Task<ExchangeRateResponse> DownloadAsync(string currencyCode)
        {
            var httpClient = new HttpClient();
            using var response = await httpClient.GetAsync(new Uri(_url, currencyCode));
            var content = await response.Content.ReadAsStringAsync();
            return _converter.Convert(content);
        }
    }
}