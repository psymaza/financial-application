using FinancialApplication.Core.Models;
using FinancialApplication.Core.Models.Interfaces;
using FinancialApplication.Core.Repositories.Interfaces;
using FinancialApplication.Core.Utils.Interfaces;

namespace FinancialApplication.Core.Utils
{
    public class CurrencyConverter : ICurrencyConverter
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;

        public CurrencyConverter(IExchangeRateRepository exchangeRateRepository)
        {
            _exchangeRateRepository = exchangeRateRepository;
        }
        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            var rate = _exchangeRateRepository.GetCurrencyRateAsync(amount.CurrencyCode, currencyCode).GetAwaiter().GetResult();
            return new CurrencyAmount(currencyCode, amount.Amount * rate);
        }
    }
}