namespace FinancialApplication.Core.Utils.Interfaces
{
    public interface IDataConverter<out T>
    {
        T Convert(string data);
    }
}