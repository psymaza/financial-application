using FinancialApplication.Core.Models.Interfaces;

namespace FinancialApplication.Core.Utils.Interfaces
{
    public interface ICurrencyConverter
    {
        ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode); 
    }
}