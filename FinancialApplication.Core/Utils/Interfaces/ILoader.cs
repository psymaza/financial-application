using System.Threading.Tasks;

namespace FinancialApplication.Core.Utils.Interfaces
{
    public interface ILoader<T1, in T2>
    {
        Task<T1> DownloadAsync(T2 args);
    }
}