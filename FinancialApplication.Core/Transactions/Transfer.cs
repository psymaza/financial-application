using System;
using FinancialApplication.Core.Models.Interfaces;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Transactions
{
    public class Transfer : ITransaction
    {
        public Transfer(ICurrencyAmount amount, DateTimeOffset date, string destination, string message)
        {
            Amount = amount;
            Date = date;
            Destination = destination;
            Message = message;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }

        public string Destination { get; }
        public string Message { get; }

        public override string ToString() => $"Перевод {Amount} на имя {Destination} с сообщением {Message}";
    }
}