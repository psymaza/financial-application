using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FinancialApplication.Core.Models;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Transactions
{
    public class TransactionParser : ITransactionParser
    {
        public ITransaction Parse(string input)
        {
            var date = DateTimeOffset.Now;
            var splits = input.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            var typeCode = splits[0];
            var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));
            
            switch (typeCode.ToLower())
            {
                case "трата":
                    return new Expense(currencyAmount, date, splits[3], splits[4]);
                case "зачисление":
                    return new Income(currencyAmount, date, splits[3]);
                case "перевод":
                    return new Transfer(currencyAmount, date, splits[3], splits[4]);

                default:
                    throw new NotImplementedException();
            }
        }
    }
}