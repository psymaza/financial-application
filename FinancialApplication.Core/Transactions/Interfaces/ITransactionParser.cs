namespace FinancialApplication.Core.Transactions.Interfaces
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input); 
    }
}