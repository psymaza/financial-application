using System;
using FinancialApplication.Core.Models.Interfaces;

namespace FinancialApplication.Core.Transactions.Interfaces
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }
        ICurrencyAmount Amount { get; }
    }
}