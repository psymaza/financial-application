using System;
using FinancialApplication.Core.Models.Interfaces;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Transactions
{
    public class Comission : ITransaction // декоратор
    {
        public ITransaction OriginalTransaction { get; }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }

        public Comission(ICurrencyAmount amount, DateTimeOffset date, ITransaction originalTransaction)
        {
            Amount = amount;
            Date = date;
            OriginalTransaction = originalTransaction;
        }

        public override string ToString() 
            => $"Комиссия в размере {Amount} за транзакцию: {OriginalTransaction}";
    }
}