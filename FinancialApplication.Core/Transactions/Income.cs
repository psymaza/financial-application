using System;
using FinancialApplication.Core.Models.Interfaces;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Transactions
{
    public class Income : ITransaction
    {
        public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
        {
            Amount = amount;
            Date = date;
            Source = source;
        }
        
        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Source { get; }

        public override string ToString() 
            => $"Зачисление {Amount} от {Source}";
    }
}