using FinancialApplication.Core.Models.Interfaces;

namespace FinancialApplication.Core.Models
{
    public class CurrencyRate : IRate
    {
        public string Code { get; set; }
        public decimal Rate { get; set; }
    }
}