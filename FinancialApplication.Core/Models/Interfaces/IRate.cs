namespace FinancialApplication.Core.Models.Interfaces
{
    public interface IRate
    {
        public string Code { get; set; }
        public decimal Rate { get; set; }
    }
}