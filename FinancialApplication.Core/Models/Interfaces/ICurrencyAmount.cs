namespace FinancialApplication.Core.Models.Interfaces
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get; }
        decimal Amount { get; } 
    }
}