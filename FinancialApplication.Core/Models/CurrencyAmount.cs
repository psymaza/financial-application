using System;
using System.Collections.Generic;
using FinancialApplication.Core.Models.Interfaces;

namespace FinancialApplication.Core.Models
{
    public class CurrencyAmount : ICurrencyAmount, IEquatable<CurrencyAmount>
    {
        public string CurrencyCode { get; }

        public decimal Amount { get; }

        public CurrencyAmount(string currencyCode, decimal amount)
        {
            CurrencyCode = currencyCode;
            Amount = amount;
        }

        public static CurrencyAmount operator +(CurrencyAmount x, ICurrencyAmount y)
            => x.CurrencyCode != y.CurrencyCode
                ? throw new InvalidOperationException("Currencies should be equal")
                : new CurrencyAmount(x.CurrencyCode, x.Amount + y.Amount);

        public static CurrencyAmount operator -(CurrencyAmount x, ICurrencyAmount y)
            => x.CurrencyCode != y.CurrencyCode
                ? throw new InvalidOperationException("Currencies should be equal")
                : new CurrencyAmount(x.CurrencyCode, x.Amount - y.Amount);

        public static bool operator ==(CurrencyAmount left, CurrencyAmount right)
            => EqualityComparer<CurrencyAmount>.Default.Equals(left, right);

        public static bool operator !=(CurrencyAmount left, CurrencyAmount right)
            => !(left == right);

        public override string ToString()
            => $"{Amount:0.00} {CurrencyCode}";

        public override bool Equals(object obj) =>
            Equals(obj as CurrencyAmount);

        public bool Equals(CurrencyAmount other)
            => other != null &&
               CurrencyCode == other.CurrencyCode &&
               Amount == other.Amount;

        public override int GetHashCode()
            => HashCode.Combine(CurrencyCode, Amount);
    }
}