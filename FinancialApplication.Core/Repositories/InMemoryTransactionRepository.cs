using System.Collections.Generic;
using FinancialApplication.Core.Repositories.Interfaces;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Repositories
{
    public class InMemoryTransactionRepository : ITransactionRepository
    {
        private readonly List<ITransaction> _transactions = new List<ITransaction>();

        public void AddTransaction(ITransaction transaction)
        {
            _transactions.Add(transaction);
        }

        public ITransaction[] GetTransactions()
            => _transactions.ToArray();
    }
}