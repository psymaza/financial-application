using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Repositories.Interfaces
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction);
        ITransaction[] GetTransactions(); 
    }
}