using System.Threading.Tasks;

namespace FinancialApplication.Core.Repositories.Interfaces
{
    public interface IExchangeRateRepository
    {
        Task<decimal> GetCurrencyRateAsync(string currencyCodeFrom, string currencyCodeTo);
    }
}