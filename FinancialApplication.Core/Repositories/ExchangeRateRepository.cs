using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinancialApplication.Core.Models;
using FinancialApplication.Core.Repositories.Interfaces;
using FinancialApplication.Core.Transactions;
using FinancialApplication.Core.Utils.Interfaces;

namespace FinancialApplication.Core.Repositories
{
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private readonly ILoader<ExchangeRateResponse, string> _exchangeRateLoader;
        private Dictionary<string, Dictionary<string, decimal>> _rates;

        public ExchangeRateRepository(ILoader<ExchangeRateResponse, string> exchangeRateLoader)
        {
            _exchangeRateLoader = exchangeRateLoader;
        }

        public async Task<decimal> GetCurrencyRateAsync(string currencyCodeFrom, string currencyCodeTo)
        {
            if (string.IsNullOrEmpty(currencyCodeFrom) || string.IsNullOrEmpty(currencyCodeTo))
                throw new InvalidOperationException("Currency code cannot be null or empty");
            if (currencyCodeFrom.Equals(currencyCodeTo))
                return 1;

            var tryGetRate = GetCurrencyRateFunc(currencyCodeFrom, currencyCodeTo);
            return tryGetRate(_rates.TryGetValue(currencyCodeFrom, out var currencyRates)
                ? currencyRates
                : await DownloadRateAsync(currencyCodeFrom));
        }

        private static Func<Dictionary<string, decimal>, decimal> GetCurrencyRateFunc(string currencyCodeFrom,
            string currencyCodeTo)
            => currencyRates => currencyRates.TryGetValue(currencyCodeTo, out var rate)
                ? rate
                : throw new InvalidOperationException($"Unknown currency code {currencyCodeFrom}");

        private async Task<Dictionary<string, decimal>> DownloadRateAsync(string currencyCode)
        {
            var result = await _exchangeRateLoader.DownloadAsync(currencyCode);
            _rates.Add(currencyCode, result.Rates);
            return result.Rates;
        }
    }
}