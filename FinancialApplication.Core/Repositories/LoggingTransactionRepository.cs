using System.Diagnostics;
using FinancialApplication.Core.Repositories.Interfaces;
using FinancialApplication.Core.Transactions.Interfaces;

namespace FinancialApplication.Core.Repositories
{
    public class LoggingTransactionRepository : ITransactionRepository // декоратор
    {
        private readonly ITransactionRepository _transactionRepository;

        public LoggingTransactionRepository(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public void AddTransaction(ITransaction transaction)
        {
            Trace.WriteLine("AddTransaction");
            _transactionRepository.AddTransaction(transaction);
        }

        public ITransaction[] GetTransactions()
        {
            Trace.WriteLine("GetTransactions");
            return _transactionRepository.GetTransactions();
        }
    }
}