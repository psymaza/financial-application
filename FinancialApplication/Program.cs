﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using FinancialApplication.Core.Models;
using FinancialApplication.Core.Repositories;
using FinancialApplication.Core.Services;
using FinancialApplication.Core.Transactions;
using FinancialApplication.Core.Utils;
using FinancialApplication.Core.Utils.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FinancialApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var accessKey = "2fefa0dfb94cf3c66831946b3c760aec";
            var transactionRepository = new LoggingTransactionRepository(new InMemoryTransactionRepository());
            var transactionParser = new TransactionParser();
            var dataConverter = new ExchangeRateConverter();
            var exchangeRateLoader = new ExchangeRateLoader($"https://exchangeratesapi.io/v1/latest?access_key={accessKey}&base=", dataConverter);
            var currencyConvertor = new CurrencyConverter(new ExchangeRateRepository(exchangeRateLoader));
            var budgetApplication = new BudgetApplication(transactionRepository, transactionParser, currencyConvertor);
            
            String s;
            if (!Console.IsInputRedirected)
            {
                s = Console.ReadLine();
                Console.WriteLine(s);
                return;
            }

            do
            {
                s = Console.ReadLine();
                budgetApplication.AddTransaction(s);
            } while (s != null);
        }
    }
}